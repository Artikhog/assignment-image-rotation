#ifndef TRANSFORM_IMAGE_H
#define TRANSFORM_IMAGE_H

#include "image.h"
#include "rotate_90.h"
#include <stdbool.h>

struct image transform(struct image f(const struct image old_image), const struct image old_image);

#endif 
