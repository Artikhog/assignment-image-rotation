#ifndef ROTATE_90_H
#define ROTATE_90_H

#include "image.h"

struct image image_rotate_90(const struct image old_image);

#endif 
