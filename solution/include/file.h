#ifndef FILE_H
#define FILE_H

#include <bits/types/FILE.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

enum open_status {
    OPEN_SUCCESSFULL = 0,
    OPEN_FAILED = 1,
};

enum close_status {
    CLOSE_SUCCESSFULL = 0,
    CLOSE_FAILED = 1,
};

enum fopen_mode {
    READ = 0,
    WRITE,
};

enum open_status open_file(char const *filename, char const *mode, FILE **file);

bool open_handler(char const *filename, enum fopen_mode mode, FILE **file);

enum close_status close_file(FILE **file);

bool close_handler(char const *filename, FILE **file);

#endif 

