#ifndef IMAGE_HANDLERS_H
#define IMAGE_HANDLERS_H

#include "bmp.h"
#include "image.h"
#include <bits/types/FILE.h>
#include "stdint.h"
#include <stdbool.h>
#include <malloc.h>

bool to_image_handler(enum image_status f(FILE *file, struct image *image), FILE *file, struct image *image);

bool from_image_handler(enum image_status f(FILE *file, struct image const *image), FILE *file, struct image *image);

#endif 
