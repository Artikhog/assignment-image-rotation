#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>

#define BM_IDENTIFIER 0x4D42
#define PLANES_NUM 1
#define INFO_HEADER_SIZE 40
#define TYPE (19778)
#define BITCOUNT (24)
#define BISIZE (40)
#define BOFFBITS (54)

enum image_status from_bmp(FILE *file, struct image *image);

enum image_status to_bmp(FILE *file, struct image const *image);

#endif

