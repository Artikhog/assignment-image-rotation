#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <stdio.h>

bool check_input_arguments(int argc);

#endif 
