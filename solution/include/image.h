#ifndef IMAGE_H
#define IMAGE_H

#include <bits/types/FILE.h>
#include "stdint.h"
#include <stdbool.h>
#include <malloc.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};
enum image_status {
    IMAGE_SUCCESSFULL = 0,
    IMAGE_INVALID_HEADER,
    IMAGE_INCORRECT_PATH,
    IMAGE_READ_DATA_ERROR,
    IMAGE_READ_HEADER_ERROR,
    IMAGE_WRITE_HEADER_ERROR,
    IMAGE_WRITE_DATA_ERROR,
    IMAGE_INVALID_SIGNATURE,
    IMAGE_ERROR,
};

void image_create(uint64_t width, uint64_t height, struct image *img);

void free_image(struct image *img);

#endif 

