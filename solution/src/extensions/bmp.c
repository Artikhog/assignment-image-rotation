#include "bmp.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint32_t calculate_padding(uint32_t imageWidth) {
    return (4 - ((3 * imageWidth) % 4)) % 4;
}

static bool check_header(struct bmp_header const *bmpHeader) {
    return bmpHeader->bfType == BM_IDENTIFIER
           && bmpHeader->biSize == INFO_HEADER_SIZE
           && bmpHeader->biPlanes == PLANES_NUM
           && bmpHeader->bOffBits >= sizeof(struct bmp_header)
           && bmpHeader->biBitCount == sizeof(struct pixel) * 8;
}

struct bmp_header create_header(struct image const *img) {
    return (struct bmp_header) {
            .bfType = TYPE,
            .bfileSize = (BOFFBITS + img->height * img->width * sizeof(struct pixel) + img->height * calculate_padding(img->width)),
            .bOffBits = BOFFBITS,
            .biSize = BISIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biBitCount = BITCOUNT,
            .biSizeImage = img->height * img->width * sizeof(struct pixel) + calculate_padding(img->width) * img->height,
    };

}


static enum image_status read_header(struct bmp_header *bmpHeader, FILE *file) {
    fread(bmpHeader, sizeof(struct bmp_header), 1, file);
    if (ferror(file) == 0 && check_header(bmpHeader)) {
        return IMAGE_SUCCESSFULL;
    } else {
        return IMAGE_INVALID_HEADER;
    }
}

enum image_status from_bmp(FILE *file, struct image *image) {
    if (file == NULL) return IMAGE_INCORRECT_PATH;

    const struct image back_image = *image;
    
    struct bmp_header bmpHeader;
    if (read_header(&bmpHeader, file) != IMAGE_SUCCESSFULL) return IMAGE_READ_HEADER_ERROR;

    if (fseek(file, bmpHeader.bOffBits, SEEK_SET) != 0) return IMAGE_INVALID_HEADER;

    image_create(bmpHeader.biWidth, bmpHeader.biHeight, image);

    const uint32_t padding_num = calculate_padding(image->width); 

    for (uint32_t i = 0; i < image->height; i++) {
        fread(image->data + i * image->width, sizeof(struct pixel), image->width, file);
        if (ferror(file) != 0) { 
            *image = back_image;
            return IMAGE_READ_DATA_ERROR;
        }
        if (fseek(file, padding_num, SEEK_CUR) != 0) {
            *image = back_image;
            return IMAGE_ERROR;
        }
    }
    return IMAGE_SUCCESSFULL;
}


enum image_status to_bmp(FILE *file, struct image const *image) {
    if (!file) return IMAGE_INCORRECT_PATH;
    
    struct bmp_header bmpHeader = create_header(image);
    fwrite(&bmpHeader, sizeof(struct bmp_header), 1, file);
    if (ferror(file) != 0) return IMAGE_WRITE_HEADER_ERROR;
    
    const char garb[] = {0, 0, 0}; 
    uint8_t padding = calculate_padding(image->width); 

    for (uint32_t i = 0; i < image->height; i++) {
        if (fwrite(image->data + i * image->width, sizeof(struct pixel), image->width, file) != image->width) return IMAGE_WRITE_DATA_ERROR;;
        fwrite(garb, 1, padding, file);
        if (ferror(file) != 0) { 
            return IMAGE_WRITE_DATA_ERROR;
        }
    }
    return IMAGE_SUCCESSFULL;
}
