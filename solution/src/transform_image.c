#include "transform_image.h"


struct image transform(struct image f(const struct image old_image), const struct image old_image) {
    fprintf(stderr, "Start transform image\n");
    struct image new_image = f(old_image);
    fprintf(stderr, "Image transformed succesfull\n");
    return new_image;
}
