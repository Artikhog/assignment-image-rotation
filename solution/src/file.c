#include "file.h"


static const char* const opening_messages[] = {
    [OPEN_SUCCESSFULL] = "File opened\n",
    [OPEN_FAILED] = "Opening error!\n"
};

static const char* const closing_messages[] = {
    [CLOSE_SUCCESSFULL] = "File closed\n",
    [CLOSE_FAILED] = "Closing error\n"
};

const char* fopen_mode_char[] = {
    [READ] = "rb",
    [WRITE] = "wb"
};

enum open_status open_file(char const *filename, char const *mode, FILE **file) {
    *file = fopen(filename, mode);
    if (*file!=NULL) {
        return OPEN_SUCCESSFULL;
    }
    return OPEN_FAILED;
}

bool open_handler(char const *filename, enum fopen_mode mode, FILE **file) {
    enum open_status status = open_file(filename, fopen_mode_char[mode], file);
    fprintf(stderr, "<%s>: %s", filename, opening_messages[status]);
    return status != OPEN_SUCCESSFULL;
}

enum close_status close_file(FILE **file) {
    if (*file) {
        fclose(*file);
        return CLOSE_SUCCESSFULL;
    }
    return CLOSE_FAILED;
}

bool close_handler(char const *filename, FILE **file) {
    enum close_status status = close_file(file);
    fprintf(stderr, "<%s>: %s", filename, closing_messages[status]);
    return status != CLOSE_SUCCESSFULL;
}
