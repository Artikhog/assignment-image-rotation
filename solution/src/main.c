#include "image.h"
#include "bmp.h"
#include "file.h"
#include "image_handlers.h"
#include "transform_image.h"
#include "utils.h"
#include <stdio.h>

int main(int argc, char **argv) {
    (void) argc; (void) argv;

    if (check_input_arguments(argc)) {
        fprintf(stdout, "Wrong arguments");
        return 1;
    }
    const char *input_path = argv[1];
    const char *output_path = argv[2];

    FILE *input_file;
    if (open_handler(input_path, READ, &input_file)) {
        fprintf(stdout, "Can't open file");
        return 1;
    }
    struct image image;
    if(to_image_handler(from_bmp, input_file, &image)) {
        fprintf(stdout, "Can't parse to image");
        return 1;
    }
    if(close_handler(input_path, &input_file)) {
        fprintf(stdout, "Can't close file");
        return 1;
    }
    struct image transformed_image = transform(image_rotate_90, image);

    free_image(&image);

    FILE *output_file;
    if (open_handler(output_path, WRITE, &output_file)) {
        fprintf(stdout, "Can't open file");
        return 1;
    }

    if(from_image_handler(to_bmp, output_file, &transformed_image)) {
        fprintf(stdout, "Can't parse to bmp file");
        return 1;
    }

    free_image(&transformed_image);

    if(close_handler(output_path, &output_file)) {
        fprintf(stdout, "Can't close file");
        return 1;
    }

    return 0;
}


