#include "rotate_90.h"

struct image image_rotate_90(const struct image old_image) {
    struct image new_image;
    uint64_t width = old_image.width;
    uint64_t height = old_image.height;
    image_create(height, width, &new_image);

    for (uint32_t i = 0; i < height; i++) {
        for (uint32_t j = 0; j < width; j++) {
            new_image.data[j * height + (height - 1 -i)] = old_image.data[i * old_image.width + j];
        }
    }
    return new_image;
}
