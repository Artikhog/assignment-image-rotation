#include "image_handlers.h"

const char* image_messages[] = {
    [IMAGE_INCORRECT_PATH] = "Incorrect path",
    [IMAGE_INVALID_HEADER] = "Invalid header",
    [IMAGE_SUCCESSFULL] = "Successful",
    [IMAGE_READ_DATA_ERROR] = "Unable to read data",
    [IMAGE_READ_HEADER_ERROR] = "Unable to read header",
    [IMAGE_WRITE_HEADER_ERROR] = "Unable to write header",
    [IMAGE_WRITE_DATA_ERROR] = "Unable to write data",
    [IMAGE_INVALID_SIGNATURE] = "Invalid Signature"
};

bool to_image_handler(enum image_status f(FILE *file, struct image *image), FILE *file, struct image *image) {
    fprintf(stderr, "Start read data\n");
    enum image_status status = f(file, image);
    fprintf(stderr, "%s\n", image_messages[status]);
    return status != IMAGE_SUCCESSFULL;
}

bool from_image_handler(enum image_status f(FILE *file, struct image const *image), FILE *file, struct image *image) {
    fprintf(stderr, "Start write data\n");
    enum image_status status = f(file, image);
    fprintf(stderr, "%s\n", image_messages[status]);
    return status != IMAGE_SUCCESSFULL;
}
