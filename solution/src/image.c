#include "image.h"

void image_create(uint64_t width, uint64_t height, struct image *img) {
    img->width = width;
    img->height = height;
    img->data = malloc(img->height * img->width * sizeof(struct pixel));
}

void free_image(struct image *img) {
    free(img->data);
}


